<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateKomentarJawabanTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('komentar_jawaban', function (Blueprint $table) {
            $table->increments('id');
            $table->text('description');
            $table->date('tanggal_buat'); 
            $table->unsignedInteger('profile_id');
            $table->foreign('profile_id')->references('id')->on('profile');
            $table->unsignedInteger('jawaban_id');
            $table->foreign('jawaban_id')->references('id')->on('jawaban');            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('komentar_jawaban');
    }
}
