<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLikedislikePertanyaanTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('likedislike_pertanyaan', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('votes');   
            $table->unsignedInteger('profile_id');
            $table->foreign('profile_id')->references('id')->on('profile');
            $table->unsignedInteger('pertanyaan_id');
            $table->foreign('pertanyaan_id')->references('id')->on('pertanyaan');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('likedislike_pertanyaan');
    }
}
