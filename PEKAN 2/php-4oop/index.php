<?php
include "Animal.php";
include "Frog.php";
include "Ape.php";



$sheep = new Animal("shaun",2,false);
$sheep->infoanimal();

echo "<br><br>";
$sungokong = new Ape("kera sakti",2,false);
$sungokong ->infoanimal();
echo "<br>";
$sungokong->yell();// "Auooo"

echo "<br><br>";
$kodok = new Frog("buduk",4,true);
$kodok ->infoanimal();
echo "<br>";
$kodok->jump() ; // "hop hop"
// 


// NB: Boleh juga menggunakan method get (get_name(), get_legs(), get_cold_blooded())



?>