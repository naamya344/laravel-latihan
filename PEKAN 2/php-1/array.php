<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Array</title>
    <link rel="stylesheet" type="text/css" href="style.css">
</head>
<body>
    <h1>Berlatih Array</h1>
        
    <?php 
        echo "<h3> Soal 1 </h3>";
        /* 
            SOAL NO 1
            Kelompokkan nama-nama di bawah ini ke dalam Array.
            Kids : "Mike", "Dustin", "Will", "Lucas", "Max", "Eleven" 
            Adults: "Hopper", "Nancy",  "Joyce", "Jonathan", "Murray"
        */
        $kids =array("Mike", "Dustin", "Will", "Lucas", "Max", "Eleven"); 
        $adults=array("Hopper", "Nancy",  "Joyce", "Jonathan", "Murray"); 

        print_r(array_values($kids));
        echo "<br> Array Kids: ";
        for ($i=0; $i <count($kids) ; $i++) { 
            echo $kids[$i].' , ';
        }

        echo "<br><br>";
        print_r(array_values($adults));
        echo "<br> Array adults: ";
        for ($i=0; $i <count($adults) ; $i++) { 
            echo $adults[$i].' , ';
        }
        

        
            //<--------------------------------SOAL NO 2------------------------------->

        echo "<h3> Soal 2</h3>";
        
        /* 
            SOAL NO 2
            Tunjukkan panjang Array di Soal No 1 dan tampilkan isi dari masing-masing Array.
        */
        echo "Cast Stranger Things: ";
        
        echo "<br>";
        echo "Total Kids: ".count($kids) ; // Berapa panjang array kids
        echo "<br>";
         
        
        // Lanjutkan
        echo "<ol>";
        for($x=0;$x<count($kids);$x++){
        echo "<li>".$kids[$x]."</li>";
        }
        echo "</ol>";
        
        echo "Total Adults: ".count($adults) ;// Berapa panjang array adults
        echo "<br>";
        
        
        // Lanjutkan
        echo "<ol>";
        for($x=0;$x<count($adults);$x++){
        echo "<li>".$adults[$x]."</li>";
        }
        echo "</ol>";
        
        


              //<--------------------------------SOAL NO 3------------------------------->

        /*
            SOAL No 3
            Susun data-data berikut ke dalam bentuk Asosiatif Array (Array Multidimensi)
            
            Name: "Will Byers"
            Age: 12,
            Aliases: "Will the Wise"
            Status: "Alive"

            Name: "Mike Wheeler"
            Age: 12,
            Aliases: "Dungeon Master"
            Status: "Alive"

            Name: "Jim Hopper"
            Age: 43,
            Aliases: "Chief Hopper"
            Status: "Deceased"

            Name: "Eleven"
            Age: 12,
            Aliases: "El"
            Status: "Alive"
            
        */
            $array=array(
                array(
                    'Name'      => "Will Byers",
                    'Age'       => 12,
                    'Aliases'   =>"Will the Wise",
                    'Status'    =>"Alive"
                ),

                 array(
                    'Name'      => "Mike Wheeler",
                    'Age'       =>12,
                    'Aliases'   =>"Dungeon Master",
                    'Status'    =>"Alive"
                ),
                  array(
                    
                    'Name'      =>"Jim Hopper",
                    'Age'       =>43,
                    'Aliases'   =>"Chief Hopper",
                    'Status'    =>"Deceased"
                 
                ),
                  array(
                    
                    'Name'      =>"Eleven",
                    'Age'       =>12,
                    'Aliases'   =>"El",
                    'Status'    =>"Alive"

                ),
         );
            echo "<table>";
            // echo "<ul>";
            for($i = 0; $i < count($array); $i++)
            {
                echo '<tr>';
                foreach($array[$i] as $key => $value) {
                    
                    echo '<th>'.$key .'</th>';
                    echo  '<td>'.$value .'</td>';
                    
                }
                echo '</tr>';
            }

            // echo "</ul>";
            echo "</table>";

    ?>
</body>
</html>