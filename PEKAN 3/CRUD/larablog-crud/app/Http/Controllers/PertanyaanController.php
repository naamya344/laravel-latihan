<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class PertanyaanController extends Controller{

	public function create(){
		return view('cruding.create');
	}

	public function store(Request $request){
		// dd($request)->all();
		$request->validate([
	        'judul' => 'required',
	        'description' => 'required|max:255',
	        'tanggal_buat' =>'date'
    	]);

		$query = DB::table('pertanyaan')->insert([
			"judul" => $request["judul"],
  		   	"description" => $request["description"],
  		   	"tanggal_buat" => $request["tanggal_buat"]
  		]);

        return redirect('/pertanyaan')->with("success","the table is maded!!");
	}

	public function index(){
		$tampil=DB::table('pertanyaan')->get();
		return view('cruding.index',compact('tampil'));
	}

	public function show($id){
		$tampil2=DB::table('pertanyaan')->where('id',$id)->first();
		return view('cruding.show',compact('tampil2'));
	}

	public function edit($id){
		$tampil2=DB::table('pertanyaan')->where('id',$id)->first();
		return view('cruding.edit',compact('tampil2'));
	}
	public function update($id, Request $request){
		$request->validate([
	        'judul' => 'required',
	        'description' => 'required|max:255',
	        'tanggal_buat' =>'date'
    	]);

		$query = DB::table('pertanyaan')->insert([
			"judul" => $request["judul"],
  		   	"description" => $request["description"],
  		   	"tanggal_buat" => $request["tanggal_buat"]
  		]);

        return redirect('/pertanyaan')->with("success","the Q is changed!!");
	}	
	public function destroy($id){
		$query=DB::table('pertanyaan')->where('id',$id)->delete();
		return redirect("/pertanyaan")->with("success","the Q is deleted!!")
	}
	}
