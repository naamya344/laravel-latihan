@extends('templates.master')

@section('title','Edit Pertanyaan')

@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
	        <div class="col-sm-6">
	            <h1>Questions Form</h1>
	        </div>
	        <div class="col-sm-6">
	            <ol class="breadcrumb float-sm-right">
	            <li class="breadcrumb-item"><a href="#">Home</a></li>
	            <li class="breadcrumb-item active">Questions Form</li>
	            </ol>
	        </div>
        </div>
    </div><!-- /.container-fluid -->
</section>

<!-- Main content -->
<section class="content">
    <div class="container-fluid">
      <div class="row">
        <!-- left column -->
        <div class="col-md-12">
          <!-- general form elements -->
          <div class="card card-primary">
          	<div class="card-header">
              <h3 class="card-title">Edit Pertanyaan {{$a->id}}</h3>
            </div>
			<div class="col-md-12 mt-2">
				<form role="form" action="/pertanyaan" method="POST">
					@csrf
					@method('PUT')
					<div class="box-body">
		                <div class="form-group">
		                  <label for="judul">Judul</label>
		                  <input type="text" class="form-control" name='judul' id="judul" placeholder="isi-judul" value="{{old('title',$tampil2->judul)}}">
		                  @error('judul')
		                    <div class="alert alert-danger">{{ $message }}</div>
		                  @enderror
		                </div>
		                
		                <div class="form-group" >
		                  <label for="description">Question</label>
		                  <textarea rows="2" class="form-control" name='description' id="description" placeholder="isi-pertanyaan">{{old('description',$tampil2->description)}}</textarea>
		                  @error('description')
		                    <div class="alert alert-danger">{{ $message }}</div>
		                  @enderror
		                </div>

		                <div class="form-group">
		                  <label for="tanggal_buat">Tanggal Dibuat</label>
		                  <input type="date" class="form-control" id="tanggal_buat" name="tanggal_buat" value="{{old('tanggal_buat',$tampil2->tanggal_buat)}}">
		                </div>
		            </div>
		             <!-- /.box-body -->
			          <div class="card-footer">
			                <button type="submit" class="btn btn-primary">Ubah</button>
			                <!-- <input type="submit" value="Submit" class="btn btn-primary"> -->
			          </div>
			    </form>
			</div>
		  </div>
	    </div>
	  </div>
	</div>
</section>
@endsection