@extends('templates.master')

@section('title','Show Pertanyaan')

@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
	        <div class="col-sm-6">
	            <h1>Form Pertanyaan</h1>
	        </div>
	        <div class="col-sm-6">
	            <ol class="breadcrumb float-sm-right">
	            <li class="breadcrumb-item"><a href="#">Home</a></li>
	            <li class="breadcrumb-item active">Questions Form</li>
	            </ol>
	        </div>
        </div>
    </div><!-- /.container-fluid -->
</section>

<!-- Main content -->
<section class="content">
    <div class="container-fluid">
      <div class="row">
        <!-- left column -->
        <div class="col-md-12">
          <!-- general form elements -->
          <div class="card card-primary">
          	<div class="card-header">
              <h3 class="card-title">Show Pertanyaan {{$tampil2 -> id}}</h3>
            </div>
			<div class="card-body">
		                <div class="form-group">
		                  <label for="judul">Judul</label>
		                  <input type="text" class="form-control" name='judul' id="judul" value="{{old($tampil2 ->judul)}}" >
		                </div>
		                
		                <div class="form-group" >
		                  <label for="description">Question</label>
		                  <textarea rows="2" class="form-control" name='description' id="description">{{old($tampil2 ->description)}}</textarea>
		                </div>

		                <div class="form-group">
		                  <label for="tanggal_buat">Tanggal Dibuat</label>
		                  <input type="date" class="form-control" id="tanggal_buat" name="tanggal_buat" value="{{old($tampil2 -> tanggal_buat)}}">
		                </div>
		            </div>
		             <!-- /.box-body -->
			          <div class="card-footer">
			                <a href="/pertanyaan" class="btn btn-info">Kembali</a>
			          </div>
			</div>
		  </div>
	    </div>
	  </div>
	</div>
</section>
@endsection