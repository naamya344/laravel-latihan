@extends('templates.master')
 
@section('title','Datatables')
 
@section('content')

<div class="mt-3 ml-3">
  <div class="card">
    <div class="card-header">
      <h3 class="card-title">Bordered Table</h3>
      <a href="/pertanyaan/create" class="btn btn-success float-right">Create A Post</a>
    </div>
    <!-- /.box-header -->
    <div class="box-body">
      @if(session('success'))
      <div class="alert alert-success">{{session('success')}}</div>
      @endif
      <table class="table table-bordered">
        <thead>
          <tr>
            <th style="width: 10px">No</th>
            <th>Title</th>
            <th>Description</th>
            <th style="width: 40px">Action</th>
          </tr>
        </thead>
        <tbody>
          @forelse($tampil as $key => $a)
            <tr>
              <td>{{ $key + 1}}</td>
              <td>{{ $a -> judul}}</td>
              <td>{{ $a -> description}}</td>
              
              <td class="d-flex">
                <a href="/pertanyaan/{{$a -> id}}" class="btn btn-info btn-sm">Show</a>
                <a href="/pertanyaan/{{$a -> id}}/edit" class="btn btn-dark">Edit</a>
                <button type="button" class="btn btn-warning">Delete</button>
                <form action="/pertanyaan/{{$a -> id}}" method="post">
                  @csrf
                  @method('DELETE')
                  <input type="submit" name="Delete" class="btn btn-danger btn-sm">
                </form>
              </td>
            </tr>
          @empty
          <tr><td colspan="4" align="center">No posts</td></tr>
          @endforelse                  
        </tbody>
      </table>
    </div>
    <!-- /.box-body -->
            <!-- <div class="box-footer clearfix">
              <ul class="pagination pagination-sm no-margin pull-right">
                <li><a href="#">«</a></li>
                <li><a href="#">1</a></li>
                <li><a href="#">2</a></li>
                <li><a href="#">3</a></li>
                <li><a href="#">»</a></li>
              </ul>
            </div> -->
  </div>
</div>
@endsection

@push('scripts')
<!-- DataTables -->
<script src="{{asset('/adminlte/plugins/datatables/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('/adminlte/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js')}}"></script>
<script src="{{asset('/adminlte/plugins/datatables-responsive/js/dataTables.responsive.min.js')}}"></script>
<script src="{{asset('/adminlte/plugins/datatables-responsive/js/responsive.bootstrap4.min.js')}}"></script>
<script>
    $(function () {
      $("#example1").DataTable()
    });
</script>
@endpush
