<!DOCTYPE html>
<html>
<head>
	<title>Form Account</title>
</head>
<body>
<h1>Buat Account Baru!</h1>
<h2>Sign Up Form</h2>

<div>
<form action="/welcome" method="POST" autocomplete="off">
	@csrf
	<label for="firstnama">First name:</label>
	<br><br>
	<input type="text" name="firstnama" placeholder="pertama" >
	<br><br>
	<label for="lastnama">Last name:</label>
	<br><br>
	<input type="text" name="lastnama" placeholder="terkhir" >
	<br><br>
	<label>Gender :</label>
	<br><br>
	<input type="radio" name="Gender" value="0">Male <br>
	<input type="radio" name="Gender" value="1">Female <br>
	<input type="radio" name="Gender" value="2">Other <br>
	<br>	
	<label>Nationality :</label>
	<br><br>
	<select>
		<option value="Indonesia">Indonesia</option>
		<option value="Australia">Australia</option>
	</select>
	<br><br>
	<label>Languange Spoken:</label>
	<br><br>
	<input type="checkbox" name="">Bahasa Indonesia<br>
	<input type="checkbox" name="">English<br>
	<input type="checkbox" name="">Others<br><br>
	<label>Bio:</label><br><br>
	<textarea cols="30" rows="10"></textarea><br>
	<input type="submit" value="Sign Up"> 
</form>	
</div>	


</body>
</html>